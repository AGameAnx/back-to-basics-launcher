
#include "targetver.h"
#include "resource.h"

#include "../Common/Common.h"

#include <chrono>
#include <thread>

static const wchar_t* s_kszLauncherAppId = L"1527290";
static const uint32_t s_knTimeout = 20u;
static const uint32_t s_knRetryTimeMillis = 100u;

int APIENTRY wWinMain(_In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPWSTR lpCmdLine, _In_ int nCmdShow)
{
	std::this_thread::sleep_for(std::chrono::seconds(1u));

	bool bFound = false;
	std::chrono::system_clock::time_point cRetryEnd(std::chrono::system_clock::now() + std::chrono::seconds(s_knTimeout));

	while (std::chrono::system_clock::now() <= cRetryEnd)
	{
		if (DWORD nProcessId = GetProcessIdByName(k_kszCOHProcessName))
		{
			bFound = true;

			DWORD nForegroundProcessId;
			GetWindowThreadProcessId(GetForegroundWindow(), &nForegroundProcessId);
			if (nProcessId == nForegroundProcessId)
				break;
		}
		std::this_thread::sleep_for(std::chrono::milliseconds(s_knRetryTimeMillis));
	}

	if (bFound)
	{
		ProcessQuickStartInfo sSteamQuickStartInfo;
		GetSteamQuickStartInfo(sSteamQuickStartInfo);
		
		sSteamQuickStartInfo.m_strCommandLine += std::wstring(L" -silent -applaunch ") + std::wstring(s_kszLauncherAppId) + L" " + k_kszDaemonModeArg;

		QuickStartProcess(sSteamQuickStartInfo);
	}

	return 0;
}
